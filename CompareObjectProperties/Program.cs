﻿using System;

namespace CompareObjectProperties
{
    class A
    {
        public string a { get; set; }
        public string b { get; set; }
        public string c { get; set; }
    }

    class B
    {
        public string a { get; set; }
        public string b { get; set; }
        public string c { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {

            var classA = new A { a = "1", b = "2", c = "3" };
            var classB = new B { a = "1", b = "2", c = "3" };

            if (classA.ComparePropertiesTo(classB))
                Console.WriteLine("Ok");
            else
                Console.WriteLine("Not Equal");

            Console.ReadLine();
        }

       
    }

    public static class ObjectComparing
    {
        public static bool ComparePropertiesTo(this Object a, Object b)
        {
            System.Reflection.PropertyInfo[] properties = a.GetType().GetProperties();
            foreach (var property in properties)
            {
                var propertyName = property.Name;
                var aValue = a.GetType().GetProperty(propertyName).GetValue(a, null);
                object bValue;
                try
                {
                    bValue = b.GetType().GetProperty(propertyName).GetValue(b, null);
                }
                catch
                {
                    return false;
                }

                if (aValue == null && bValue == null)
                    continue;
                if (aValue == null && bValue != null)
                    return false;
                if (aValue != null && bValue == null)
                    return false;

                if (aValue.GetHashCode() != bValue.GetHashCode())
                    return false;

            }


            return true;
        }

        public static bool CompareFieldsTo(this Object a, Object b)
        {
            System.Reflection.FieldInfo[] fields = a.GetType().GetFields();
            foreach (var field in fields)
            {
                var fieldName = field.Name;
                var aValue = a.GetType().GetField(fieldName).GetValue(a);
                object bValue;
                try
                {
                    bValue = b.GetType().GetField(fieldName).GetValue(b);
                }
                catch
                {
                    return false;
                }

                if (aValue == null && bValue == null)
                    continue;
                if (aValue == null && bValue != null)
                    return false;
                if (aValue != null && bValue == null)
                    return false;

                if (aValue.GetHashCode() != bValue.GetHashCode())
                    return false;

            }


            return true;
        }
    }
}
